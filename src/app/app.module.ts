import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HeroService } from './services/hero.service';
import { HeroDetailComponentComponent } from './hero-detail-component/hero-detail-component.component';
import { HeroesComponent } from './heroes/heroes.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { UserService } from './services/user.service';

@NgModule({
  declarations: [
    AppComponent,
    HeroDetailComponentComponent,
    HeroesComponent,
    DashboardComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full',
      },
      {
        path: 'heroes',
        component: HeroesComponent
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
      },
      {
        path: 'detail/:id',
        component: HeroDetailComponentComponent,
      },
      {
        path: 'login',
        component: LoginComponent,
      }
    ]),
  ],
  providers: [
    HeroService,
    UserService,
  ],
  bootstrap: [AppComponent],
})

export class AppModule { }
