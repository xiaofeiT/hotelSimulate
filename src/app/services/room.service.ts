import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { Room } from '../classes/room';

@Injectable()
export class RoomService {

  private getAllRoomsUrl = ''; //  get all the rooms
  private getRoomByIdUrl = '';   // get the room by id
  private getRoomByPriceUrl = '';  //  get the room by price
  private getRoomBynumUrl = '';  //   get the room by the number of the bed
  private getRoomByLevelUrl = '';  //  get the room by the level of the room
  private addRoomUrl = '';  //  add a room, a room: roomId, bed number, price, level
  private updateRoomUrl = ''; //  update a room's info

  constructor(private http: Http) { }

  private getAllRoom(): Promise<Room> {
    return this.http.get(this.getAllRoomsUrl)
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  private getRoomById(id: number): Promise<Room> {
    const url = '${this.getRoomByIdUrl}/${id}';
    return this.http.get(url)
      .toPromise()
      .then( res => res.json().data as Room)
      .catch(this.handleError);
  }

  private getRoomByPrice(price: number): Promise<Room[]> {
    const url = '${this.getRoomByPriceUrl}/${price}'; //  find the rooms that have the same price
    return this.http.get(url)
      .toPromise()
      .then( res => res.json().data as Room[])
      .catch(this.handleError);
  }

  private getRoomByNum(num: number): Promise<Room[]> {
    const url = '${this.getRoomByPriceUrl}/${num}';
    return this.http.get(url)
      .toPromise()
      .then(res => res.json().data as Room[])
      .catch(this.handleError);
  }

  private getRoomByLevel(level: number): Promise<Room[]> {
    const url = '${this.getRoomByLevelUrl}/${level}';
    return this.http.get(url)
      .toPromise()
      .then( res => res.json().data as Room[])
      .catch(this.handleError);
  }

  private  addRoom(room: Room): Promise<Room> {
    return this.http.post(this.addRoomUrl, JSON.stringify(room))
      .toPromise()
      .then(res => res.json().data as Room)
      .catch(this.handleError);
  }

  private updateRoom(room: Room): Promise<Room> {
    return this.http.put(this.updateRoomUrl, JSON.stringify(room))
      .toPromise()
      .then(res => res.json().data as Room)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.log('An error occured in room service', error);
    return Promise.reject(error.message || error);
  }

}
