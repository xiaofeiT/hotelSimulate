import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { Guest } from '../classes/guest';

@Injectable()
export class GuestService {

  private addGuestUrl = ''; // add guest
  private deleteGuestByIdUrl = ''; // delete one guest by id
  private deleteGuesrByNameUrl = '';  //  delete one guest by name
  private getAllGuestUrl = '';  //  get all the guest
  private getGuestByNameUrl = '';  //  get one guest by name
  private getGuestByIdUrl = '';   // get one guest by id
  private updateGuestByIdUrl = ''; //  update one guest by id
  private updateGuestByNameUrl = '';  // updaye one guest by name

  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) { }

  private addGuest(name: string, id: number): Promise<Guest> {
    return this.http.post(this.addGuestUrl, JSON.stringify({name: name, id: id}), {headers: this.headers})
      .toPromise()
      .then(response => response.json().data as Guest)
      .catch(this.handleError);

  }

  private deleteGuestById(name: string): Promise<void> {
    const url = '${this.deleteGusetByIdUrl}/${id}';
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  private deleteGuestByName(name: string): Promise<null> {
    const url = '${this.deleteGuestByNameUrl}/${name}';
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  private getAllGuest(): Promise<Guest[]> {
    return this.http.get(this.getAllGuestUrl)
      .toPromise()
      .then(res => res.json().data as Guest[])
      .catch(this.handleError);
  }

  private getGuestByName(): Promise<Guest[]> {
    const url = '${this.getGuestByNameUrl}/${name}';
    return this.http.get(url)
      .toPromise()
      .then(res => res.json().data as Guest[])
      .catch(this.handleError);
  }

  private getGuestById(): Promise<Guest> {
    const url = '${this.getGuestByIdUrl}/${id}';
    return this.http.get(url)
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  private updateGuestByName(guest: Guest): Promise<Guest> {
    const url = '${this.updateGuestByNameUrl}/${guest.name}';
    return this.http.put(url, JSON.stringify(guest), {headers: this.headers})
      .toPromise()
      .then(() => guest)
      .catch(this.handleError);
  }

  private updateGuestById(guest: Guest): Promise<Guest> {
    const url = '${this.updateGuestByIdUrl}/${guest.id}';
    return this.http.put(url, JSON.stringify(guest))
      .toPromise()
      .then(() => guest)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.log('An error ocuured', error);
    return Promise.reject(error.message || error);
  }

}
