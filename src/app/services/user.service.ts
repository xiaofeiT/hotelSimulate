import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { User } from '../classes/user';

@Injectable()
export class UserService {

  private getAllUserUrl = 'http://localhost:3000/user'; // get all the user
  private getUserByIdUrl = 'http://localhost:3000/user'; //  get user's name and info by id
  private getUserByNameUrl = 'http://localhost:3000/user'; // get user's name and info by name
  private getPasswdByIdUrl = 'http://localhost:3000/user'; //  get user's passwd by user's id
  private getPasswdByNameUrl = 'http://localhost:3000/user'; //  get user's passwd by user's name
  private getUserNameByIdUrl = 'http://localhost:3000/user'; //  get user's name by user's id
  private getUserIdByNameUrl = 'http://localhost:3000/user'; //  get user's id bu user's name

  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) { }

  searchByName(name: string): Observable<User> {
    return this.http.get( `${this.getUserByNameUrl}/?name=${name}`)
      .map(res => res.json().data as User);
  }

  getAllUser(): Promise<User[]> {
    return this.http.get(this.getAllUserUrl)
      .toPromise()
      .then(response => response.json().data as User[])
      .catch(this.handleError);
  }

  getUserById(id: string): Promise<User>  {
    const url = `${this.getUserByIdUrl}/${id}`;
    return this.http.get(url)
        .toPromise()
        .then(res => res.json() as User)
        .catch(this.handleError);
  }

  getUserByName(name: string): Promise<User> {
    const url = `${this.getUserByNameUrl}/${name}`;
    return this.http.get(url)
        .toPromise()
        .then(response => response.json() as User)
        .catch(this.handleError);
  }

  getPasswdById(id: number): Promise<string> {
    const url = `${this.getPasswdById}/${id}`;
    return this.http.get(url)
        .toPromise()
        .then(response => response.json() as string)
        .catch(this.handleError);
  }

  getPasswdByName(name: string): Promise<string> {
    const url = `${this.getPasswdByName}/${name}`;
    return this.http.get(url)
        .toPromise()
        .then(response => response.json() as string)
        .catch(this.handleError);
  }

  getUserNameById(id: number): Promise<string> {
    const url = `${this.getUserNameById}/${id}`;
    return this.http.get(url).
        toPromise()
        .then(response => response.json() as string)
        .catch(this.handleError);
  }

  getUserIdByName(name: string): Promise<number[]> {
    const url = `${this.getUserIdByName}/${name}`;
    return this.http.get(url)
        .toPromise()
        .then(response => response.json() as number[])
        .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occured', error);
    return Promise.reject(error.message || error);
  }

  private handleLooginError(error: any): Promise<any> {
    console.error('用户不存在', error);
    return Promise.reject(error.message || error);
  }
}
