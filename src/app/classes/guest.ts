export class Guest {
    id: number;
    name: string;
    inTime: string;
    outTime: string;
    orderRoom: number;
    bedId: number;
    classify: number; // 1 for 单住， 2 for 合住
}
