export class Room {
    roomId: number;
    bedNumber: number;
    price: number;
    level: number;
}
