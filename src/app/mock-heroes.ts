import { Hero } from './hero';

export const HEROES: Hero[] = [
    { id: 11, name: 'Mr.Nice'},
    { id: 12, name: 'Narce'},
    { id: 13, name: 'Bom'},
    { id: 14, name: 'cele'},
    { id: 15, name: 'magneta'},
    { id: 16, name: 'rubber'},
    { id: 17, name: 'dyname'},
    { id: 18, name: 'dr iq'},
    { id: 19, name: 'magma'},
    { id: 20, name: 'tornado'}
];
