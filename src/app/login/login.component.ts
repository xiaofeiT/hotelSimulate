import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from '../services/user.service';
import { User } from '../classes/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {

  user: User;
  userName: string;
  userId: string;
  userPasswd: string;
  loginButtonSty = 'layui-btn layui-btn-disabled layui-col-md4 layui-col-md-offset4';
  loginInf = '用户名不能为空';
  useres: User[];
  use: User;

  constructor(
    private userService: UserService,
    private router: Router,
  ) {
  }

  ngOnInit() {
  }

  login(userId, userPasswd): void {
    if (userId === undefined || userPasswd === undefined) {
      // can't login
      console.log('error');
    } else {
      // can login
      this.userService.searchByName(userId);
      console.log(this.user);
      if ( userPasswd === this.user.passwd) {
        console.log('can login');
       } else if (userPasswd !== this.user.passwd) {
        console.log('cannot login');
       }
    }
  }

  getUserById(userId: string): Promise<User> {
    return this.userService.getUserById(userId)
      .then(user => this.user = user);
  }

  print(): void {
    console.log(this.user);
  }

  checkUserInf(): void {
    if (this.userId !== undefined) {
      if (this.userPasswd !== undefined) {
        this.loginButtonSty = 'layui-btn layui-btn-normal layui-col-md4 layui-col-md-offset4';
        this.loginInf = '登录';
      } else {
        this.loginInf = '密码不能为空';
      }
    }
  }
}
